How to run our application? 

1. Step

Download SkillEtCooking repository on your computer.
There are 2 ways of doing it: 

a) Simply download repository from this link - https://gitlab.com/vlak-u-stogu/skill-et-cooking-frontend.

b) Or clone the repository using GIT - git clone.

2. Step

Download and setup Node.js.
- npm install
- npm run serve
- npm run build

3. Step

Open CMD and locate yourself in the src file of your downloaded repository.
Then type npm start and wait a moment and your app should be started!

Another way of getting access to our page is through this link - https://skill-et-cooking.herokuapp.com/.


