import "./App.css";

import React, { useState, useEffect } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Welcome from "./components/Welcome";
import LogInForm from "./components/LogInForm";
import RegisterForm from "./components/RegisterForm";
import Header from "./components/Header";
import DisqusTest from "./components/DisqusTest";
import AddRecipe from "./components/AddRecipe";
import RecipePage from "./components/RecipePage";
import Users from "./components/Users";
import UpdateRecipe from "./components/UpdateRecipe";
import DisqusTest2 from "./components/DisqusTest2";
import FromAuthor from "./components/FromAuthor";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [isModerator, setIsModerator] = useState(false);

  const [users, setUsers] = useState([]);

  /*useEffect(() => {
    fetch("https://skill-et-cooking-backend.herokuapp.com/users")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        //console.log(data);
        setUsers(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);*/
  //auth

  function checkIsLoggedIn() {
    const options = {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Request-Headers": "Authorization, X-PING",
        "Access-Control-Request-Method": "POST, OPTIONS, GET",
        "Access-Control-Allow-Origin": "*",
      },
    };

    //https://cors-anywhere.herokuapp.com/
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/user?" +
        new URLSearchParams({
          auth: localStorage.getItem("jwt"),
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((myJson) => {
        handleLogin();
      })
      .catch((e) => {
        localStorage.clear();
        handleLogout();
      });
  }

  if (localStorage.getItem("jwt") != null) checkIsLoggedIn();

  function handleLogin() {
    setIsLoggedIn(true);
    if (localStorage.getItem("isMod") == 1) setIsModerator(true);
    else setIsModerator(false);
  }

  function handleLogout() {
    localStorage.clear();
    setIsLoggedIn(false);
    setIsModerator(false);
  }

  return (
    <BrowserRouter>
      <Header
        onLogout={handleLogout}
        loggedIn={isLoggedIn}
        moderator={isModerator}
      />
      <div className="App">
        <Switch>
          <Route
            path="/"
            exact
            component={Welcome}
            loggedIn={isLoggedIn}
            moderator={isModerator}
          />
          <Route path="/login" exact component={LogInForm} />

          <Route
            path="/register"
            exact
            component={RegisterForm}
            onRegister={handleLogin}
          />

          <Route 
            path="/RecipePage/:recipeid" 
            exact 
            component={RecipePage} 
          />

          <Route 
            path="/disqus" 
            exact 
            component={DisqusTest} 
          />

          <Route 
            path="/disqus2" 
            exact 
            component={DisqusTest2} 
          />

          <Route 
            path="/addRecipe" 
            exact 
            component={AddRecipe} 
          />

          <Route 
            path="/users" 
            exact 
            component={Users} 
          />

          <Route 
            path="/UpdateRecipe" 
            exact 
            component={UpdateRecipe} 
          />

          <Route
            path="/fromauthor/:userid"
            exact component={FromAuthor}
          />
          
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
