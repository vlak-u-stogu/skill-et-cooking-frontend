import React, { useState } from "react";
import Card from "./Card";
import "./RegisterForm.css";
import { Link } from "react-router-dom";

function RegisterForm(props) {
  const [form, setForm] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [registerFail, setRegisterFail] = useState(false);

  function onChange(event) {
    const { name, value } = event.target;
    setForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      username: form.username,
      email: form.email,
      password: form.password,
    };
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/user/register",
      options
    )
      .then((response) => {
        if (response.ok) {
          setRegisterFail(false);
          window.location.href = "/login";
        } else {
          setRegisterFail(true);
        }
      })
      .catch((err) => {
        console.log(err);

      });
  }

  function isValid() {
    const { username, email, password } = form;
    return (
      username.length >= 5 && username.length <= 30 &&
      password.length >= 8 &&
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
    );
  }

  return (
    <Card title="Get started with a free account">
      <div className="div1">
        Create a free Skill-et-cooking account and start sharing your own
        recipes! Already have an account? <Link to="/login">Log in here</Link>
      </div>
      <div className="RegisterForm">
        <form onSubmit={onSubmit}>
          <div className="FormRow">
            <label>Username: </label>
            <input name="username" onChange={onChange} value={form.username} />
          </div>
          <div className="FormRow">
            <label>Email: </label>
            <input name="email" onChange={onChange} value={form.email} />
          </div>
          <div className="FormRow">
            <label>Password: </label>
            <input
              type="password"
              name="password"
              onChange={onChange}
              value={form.password}
            />
          </div>
          {registerFail && <p>Username or email already taken!</p>}
          <button type="submit" onClick={onSubmit}
          disabled={!(
            form.username.length >= 5 &&
            form.username.length <= 30 &&
            form.password.length >= 8 && 
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.email))}>
            Register
          </button>
        </form>
      </div>
    </Card>
  );
}

export default RegisterForm;
