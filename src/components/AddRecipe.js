import React, {useState} from "react";
import Card from "./Card";
import "./AddRecipe.css";
import { Link } from "react-router-dom";


function AddRecipe(props) {
	
	const [ingredients, setIngredients] = useState([]); 
	const [quantity, setQuantity] = useState([]);

	const [submitFail, setSubmitFail] = useState(false);
	
	function addIngredient(event){
		event.preventDefault();
        console.log(ingredients);
        const ingredient = document.getElementById("ingredient1").value;
		const quantiti = document.getElementById("quantity1").value;
        console.log(ingredient);
        console.log(ingredients.indexOf(ingredient));
        if(ingredients.indexOf(ingredient) === -1){
            console.log(ingredients);
            setIngredients(old => [...old, ingredient]);
			setQuantity(old => [...old, quantiti]);
            console.log(ingredients);
        }
		else {
			let index = ingredients.indexOf(ingredient);
			setIngredients(old => old.filter(el => el != ingredient));
			setQuantity(old => old.filter((el, i) => i != index));
			setIngredients(old => [...old, ingredient]);
			setQuantity(old => [...old, quantiti]);
		}
        console.log(ingredients);
    }
	
	const [steps, setSteps] = useState([]); 
	
	function addStep(event){
		event.preventDefault();
        console.log(steps);
        const step = document.getElementById("steps1").value;
        console.log(step);
        console.log(steps.indexOf(step));
        if(steps.indexOf(step) === -1){
            console.log(steps);
            setSteps(old => [...old, step]);
            console.log(steps);
        }
        console.log(steps);
    }
	
	const [recipe, setRecipe] = useState({
		name:"",
		duration:"",
	}); 
	
	const [images, setImages] = useState([]); 
	
	function addImage(event){
		event.preventDefault();
        console.log(images);
        const image = document.getElementById("image1").value;
        console.log(image);
        console.log(ingredients.indexOf(image));
        if(ingredients.indexOf(image) === -1 ){
			if(images.length == 5){
				alert("Maximum number of images already uploaded!");
			}
			else{
				console.log(images);
				setImages(old => [...old, image]);
				console.log(images);
			}

        }
        console.log(images);
    }

	function addImages(e){
		e.preventDefault();
        setImages([...e.target.files]);
        console.log(images);
        console.log(images);
    }
	
	
  function onSubmit(e) {
    e.preventDefault();
	
	
	const lista = [];
	
	for(let i = 0;i<ingredients.length;i++){
		/*const formData2 = new FormData();
		formData2.append("name", ingredients[i]);
		formData2.append("amount", quantity[i]);
		lista.push(formData2);*/
		lista.push({"name": ingredients[i], "amount": quantity[i]});
	}
	
	
    const recipeData = {
	  name: recipe.name,
	  duration: recipe.duration,
	  ingredients: lista,
	  steps: steps,
    };

	const data = new FormData();
	/*
	data.append("recipe", recipeData);

	console.log(data);
	console.log(JSON.stringify(data));

	images.forEach( (image, index)  => data.append(index, image));

	console.log(data.get("recipe"));
	console.log(data.get("0"));
	*/

	data.append("recipe", new Blob([JSON.stringify(recipeData)], {type: "application/json"}));
	images.forEach( (image, index)  => data.append(index, image));

	
    const options = {
      method: "POST",
      //headers: {
      //  'Content-Type': 'multipart/form-data; boundary=--abc'
      //},
      body: data,
    };
	
    return fetch('https://skill-et-cooking-backend.herokuapp.com/recipes/add?' + new URLSearchParams({
    auth: localStorage.getItem("jwt")
  }), options)
      .then((response) => {
        if (response.ok) {
          window.location.href = "/";
        }
		else{
			setSubmitFail(true);
		}
      })
      .catch((err) => {
        console.log(err);
      });
  }
  
  
	function onChange(event) {
		const { name, value } = event.target;
		setRecipe((oldRecipe) => ({ ...oldRecipe, [name]: value }));
	}
	
	 function removeAllIngredients(event){
        setIngredients(old => []);
		setQuantity(old => []);
    }
	
	 function removeAllSteps(event){
        setSteps(old => []);
    }
	
	function removeAllImages(event){
        setImages(old => []);
    }
	
	return(			
		<Card title="Add your own recipe!">
				<div className="AddRecipe">
					<form onSubmit={onSubmit}>
						<div className="FormRow">
							<label>Name: </label>
							<input name="name" id="name1"  onChange={onChange} value={recipe.name} />
						</div>	
						<div className="FormRow">
							<label>Duration (in mins): </label>
							<input type="number" min = "1" name="duration" id="duration1"  onChange={onChange} value={recipe.duration} />
						</div>
						<div className="Row">
							<label>Ingredients: </label>
							<input name="ingredients" id="ingredient1" required />
							<label>Ingredient quantity: </label>
							<input type="text" name="quantity" id="quantity1" />
							<div className="row2">
							<button onClick={addIngredient}> Add/Change ingredient</button>
							<button onClick={removeAllIngredients}> Remove all</button>
							</div>
							<div>
								<p>{ingredients.length !==0 && "Currently added ingredients:"}</p>
								{ingredients.map((el) => <p>Ingredient: {el} Quantity: {quantity[ingredients.indexOf(el)]}</p>)}								
							</div>
						</div>
						<div className="FormRow">
							<label>Steps: </label>
							<textarea  rows="15" cols="100" name="steps" id="steps1" required />
							<button onClick={addStep}> Add step</button>
							<button onClick={removeAllSteps}> Remove all</button>
							<div>
								<p>{steps.length !==0 && "Currently added steps:"}</p>
								<p>{steps.map(el => <span>{el} </span>)}</p>
							</div>	
						</div>
						<div className="Row1">
							<label>Image: </label>
							<input type="file" multiple accept="image/*" onChange={addImages}/>
							{/*}
							<input
								type="file"
								name="img"
								accept="image/*"
								id="image1"
							/>
							<button onClick={addImage}> Add image</button>
							<button onClick={removeAllImages}> Remove all</button>
							<p>{images.length !==0 && "Currently added images:"}</p>
							<p>{images.map(el => <span>{el} </span>)}</p>
							{*/}
						</div>
					<button type="submit" onClick={onSubmit}>
						Add recipe
					</button>
					{submitFail && <p>Please input at least one ingredient (with quantity) and one step!</p>}
					</form>
				</div>
		</Card>
		);
}
export default AddRecipe;