import React from "react";
import "./Welcome.css";
import pic from "./images/slika3.jpg";
import pic1 from "./images/chef4.png";
import { Link } from "react-router-dom";
import {Recipes} from "./Recipes";
import SortAndFilter from "./SortAndFilter";



function Welcome(props){

    return (
	<div className="div123">
		<section className ="section">
			<img src={pic} alt="Be your own chef"/>
			<p>Cooking made easy...<br/>
				Become your own chef!
			</p>	
		</section>
		<section className ="section2">
			<h1 className = "welcomeSign">Welcome to Your Cooking Page!</h1>
			<img src={pic1} alt=" " />
		</section>

		<section className="section3" background-color="#F0F8FF">
			<SortAndFilter />
		</section>
		<hr/>
		<section className= "section3" >
		{localStorage.getItem("jwt") && localStorage.getItem("isMod")==0 && <Link to='/addRecipe'><button className= "buttonClass">Add Recipe</button></Link>}
		</section>
	</div>
	)
}

export default Welcome;