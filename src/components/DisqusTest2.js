import React, {Component} from "react";
import Disqus from "disqus-react";

export default class extends Component {
    render() {
      const disqusShortname = "skilletcooking-1"
      const disqusConfig = {
        url: "http://localhost:3000/disqus2", //potrebno promijeniti za deploy
        identifier: "2",
        title: "Testing Disqus on our project"
      }

  
      return (
        <div>
  
          <h1>Page Title</h1>
  
          <p>Page content.</p>
  
          <Disqus.DiscussionEmbed
            shortname={disqusShortname}
            config={disqusConfig}
          />
        </div>
      )
    }
  }
