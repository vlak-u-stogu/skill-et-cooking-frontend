import { disabled } from "express/lib/application";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Recipes from "./Recipes";
import pic from "./images/ingredients.png";

function SortAndFilter(props) {
  const [filterIngredients, updateFilterIngredients] = useState([]);


  useEffect(() => 
    getRecepies()
  ,[]);
  
  function getRecepies() {
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/page/1/?num=10000",
      options
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((myRecipes) => {
        setRecipes(myRecipes);
      })
      .catch((e) => {
        setRecipes([]);
        console.log("NISMO USPJELI");
        console.log(e);
      });
  }

  function handleSubmit(event) {
    console.log(event.target.sort.value);
    console.log(filterIngredients);
    event.preventDefault();
  }

  function addIngredient(event) {
    console.log(filterIngredients);
    const ingredient = document.getElementById("ingredientFilter").value;
    console.log(ingredient);
    console.log(filterIngredients.indexOf(ingredient));
    if (filterIngredients.indexOf(ingredient) === -1) {
      console.log(filterIngredients);
      updateFilterIngredients((old) => [...old, ingredient]);
      console.log(filterIngredients);
    }
    console.log(filterIngredients);
  }

  function removeIngredient(event) {
    console.log(filterIngredients);
    const ingredient = document.getElementById("ingredientFilter").value;
    console.log(ingredient);
    updateFilterIngredients((old) => old.filter((item) => item !== ingredient));
    console.log(filterIngredients);
  }

  function removeAllIngredients(event) {
    updateFilterIngredients((old) => []);
  }

  const [recipes, setRecipes] = useState([]);
  const [searchName, updateName] = useState("");
  const [sort, setSort] = useState("");
  const [search, setSearch] = useState("");
  const [sortEnable, setSortEnable] = useState(true);

  function searchForRecipesByIngredient(event) {
    event.preventDefault();

    const options = {
      method: "POST",
      body: JSON.stringify(filterIngredients),
      headers: {
        "Content-Type": "application/json",
      },
    };
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/page/1/?num=10000",
      options
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((myRecipes) => {
        setRecipes(myRecipes);
      })
      .catch((e) => {
        setRecipes([]);
        console.log("NISMO USPJELI");
        console.log(e);
      });
  }

  function changeName(event) {
    console.log(searchName);
    const newSearchName = document.getElementById("recipeName").value;
    console.log(searchName);

    updateName(newSearchName);
    console.log(searchName);
  }

  function searchForRecipesByName(event) {
    event.preventDefault();

    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/page/1/?" +
        new URLSearchParams({
          num: 10000,
          search: searchName,
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((myRecipes) => {
        setRecipes(myRecipes);
      })
      .catch((e) => {
        setRecipes([]);
        console.log("NISMO USPJELI");
        console.log(e);
      });
  }

  function sortRecipes(event) {
    event.preventDefault();

    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/page/1/?" +
        new URLSearchParams({
          num: 10000,
          sort: event.value,
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((myRecipes) => {
        setRecipes(myRecipes);
      })
      .catch((e) => {
        setRecipes([]);
        console.log("NISMO USPJELI");
        console.log(e);
      });
  }

  return (
    <div className="div123">
      <p>Here you can customise recipes shown to you!</p>
      <form onSubmit={handleSubmit}>
        <div className="div123">
          <p>
            Sort by:
            <input
              type="radio"
              id="sortPopularity"
              name="sort"
              value="popularity"
              defaultChecked
              onChange={sortRecipes}
              disabled={filterIngredients.length !== 0}
            />
            <label htmlFor="sortPopularity">Popularity</label>
            <input 
              type="radio"
              id="sortScore"
              name="sort" 
              value="score" 
              onChange={sortRecipes}
              disabled={filterIngredients.length !== 0}
            />
            <label htmlFor="sortScore">User Score</label>
            <input
              type="radio"
              id="sortRecommended"
              name="sort"
              value="recommended"
              onChange={sortRecipes}
              disabled={filterIngredients.length !== 0}
            />
            <label htmlFor="sortRecommended">Recommended</label>
          </p>
        </div>
        <div className="div123">
          <p>
            Only recipes with ingredient:
            <input type="text" id="ingredientFilter" name="filter" />
            <button onClick={addIngredient}>Add ingredient to filter</button>
            <button onClick={removeIngredient}>
              Remove ingredient from filter
            </button>
            <button onClick={removeAllIngredients}>
              Remove all ingredients
            </button>
          </p>
        </div>

        <div className="div123">
          <p>
            {filterIngredients.length !== 0 && "Currently added ingredients:"}
          </p>
          <p>
            {filterIngredients.map((el) => (
              <span>{el} </span>
            ))}
          </p>
        </div>

        <div  className="div123">
			<img src={pic} alt="ingredients"/>
          <button type="submit" onClick={searchForRecipesByIngredient}>
            Search by ingredient
          </button>
        </div>
      </form>
      <form>
        <div  className="div123">
          <p>
            Search recipes by name: 
            <input
              type="text"
              id="recipeName"
              name="name"
              onChange={changeName}
            />
          </p>
        </div>
        <div  className="div123">
          <button type="submit" onClick={searchForRecipesByName}>
            Search by name
          </button>
        </div>
      </form>
      <section className="div123">
        <Recipes recipes={recipes} />
      </section>
    </div>
  );
}

export default SortAndFilter;
