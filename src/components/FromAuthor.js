import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import "./FromAuthor.css";


function FromAuthor(props){

    let {userid} = useParams();
    const [recipes, setRecipes] = useState(null);

    useEffect(() => {
        return fetch('https://skill-et-cooking-backend.herokuapp.com/recipes?' + new URLSearchParams({
		userid: userid,	
		}))
		.then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
        .then((authorRecipes) => {
            console.log(authorRecipes);
            setRecipes(authorRecipes);
        })
      .catch((err) => {
        console.log(err);
      });
    }, []);

    return(
        <div  className="div123">
			<p className= "Recipes">
			{recipes!==null && 
            <div><p>Recipe list:</p>
			 <ul>
				{recipes.map(recipe => (
					
					 	<li key={recipe.id}>

                {recipe.pictures[0] !== undefined && <img src={recipe.pictures[0]?.picfilelocation} />}
							 <Link to={{
								pathname: "/RecipePage/"+String(recipe.recipeid),
								state: recipe,
								}}>
								 {recipe.name} ({recipe.views} views)
							</Link>
                     	</li>
				))}
			</ul>
            </div>
            }
			</p>
		</div>
    );
}

export default FromAuthor;	