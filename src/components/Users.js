import React, {Component} from "react";
import Card from "./Card";
import "./Users.css";

export class Users extends Component {
	
	constructor(props){
		super(props);
		this.state = {
			items: [],
			isLoaded: false,
		}
	}
	
	
	onSubmit(e) {
		e.preventDefault();
		const userid = document.getElementById("userDel").value;
		console.log(userid);
		const options = {
			method: "POST",
			//headers: {
				//'Content-Type': 'application/json'
			//},
			body: userid,
			};
		console.log(options);
		return fetch('https://skill-et-cooking-backend.herokuapp.com/user/delete/' + userid + '?' + new URLSearchParams({	
		auth: localStorage.getItem("jwt")
		}), options)
		.then((response) => {
        if (response.ok) {
          window.location.href = "/users";
        }
      })
      .catch((err) => {
        console.log(err);
      });
	}
	
	componentDidMount(){
		
		fetch('https://skill-et-cooking-backend.herokuapp.com/user/allUsers?' + new URLSearchParams({
			auth: localStorage.getItem("jwt")
		  }))
			.then(res => res.json())
			.then(json => {
				this.setState({
					isLoaded: true,
					items: json,
				})
			});	
	}

	render(){
		
		var{isLoaded, items} = this.state;
		if(!isLoaded){
			return <div>No users loaded.</div>
		}
		else{
			return(
			<div>
			<Card>
			<h2>List of all registered users!</h2>
			<div className ="div1">
			<ul>
				{items.map(item => (
					 <li key={item.userid}>
                            Name: {item.username} Id: {item.userid}
                     </li>
				))}
			</ul>	
			</div>
			<div className ="div1">
			<input type="number" name="userId" id="userDel"/> <button type="submit" onClick = {this.onSubmit} >
            Delete user
          </button>
			</div>
			</Card>
			</div>
		);	
		}
	}	
}

export default Users;