import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";
import icon from "./images/icon.png";

function Header(props){
    return(
        <header className = "Header">
			<img src={icon} alt=" " />
			<Link to='/'>Home</Link>
			{!props.loggedIn && <Link to='/register'><button>Register</button></Link>}
			{props.loggedIn && <a>{localStorage.getItem("username")}</a>}
			{props.loggedIn && <button onClick={props.onLogout}>Log Out</button>}
			{!props.loggedIn && <Link to='/login'><button>Log In</button></Link>}
			{props.loggedIn && props.moderator &&
			<Link to='/users'><button>Users</button></Link>}
        </header>
    );
}

export default Header;	