import React, {Component, useEffect, useState} from "react";
import "./RecipePage.css";
import Disqus from "disqus-react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom/cjs/react-router-dom.min";
import Rating from "react-rating";
import Card from "./Card";

const RecipePage = props => {
  const location = useLocation();
    var myRecipe = location.state
    console.log(location.state)
	//console.log(myRecipe.recipeid)
  
    const [isMy, updateMy] = useState(false)
    const [rating, setRating] = useState(0);
    const [rated, setRated] = useState(false);

    function changeRating(value){
      console.log(value);
      setRating(value);
    }

    function checkIfOwnRecipe() {
        var recipeAuthor = myRecipe.person.username
        var logedInUser = localStorage.getItem("username")

        if (recipeAuthor == logedInUser){
            updateMy(true)
        }
    }

    //za komentare
	const disqusShortname = "skilletcooking-1"
    const disqusConfig = {
        //https://skill-et-cooking-backend.herokuapp.com
      url: "https://skill-et-cooking-backend.herokuapp.com/RecipePage/"+String(myRecipe?.recipeid),
      identifier: "Disqus"+String(myRecipe?.recipeid),
      title: "Recipe comment section - " + myRecipe?.name
    }

    function onSubmit(e) {
		e.preventDefault();
		//const recipeid = document.getElementById("recipeDel").value;
		//console.log(recipeid);
		const options = {
			method: "POST",
			//headers: {
				//'Content-Type': 'application/json'
			//},
				//body: myRecipe.recipeid,
			};
		console.log(options);
		return fetch('https://skill-et-cooking-backend.herokuapp.com/recipes/delete?' + new URLSearchParams({
		recipeid: myRecipe.recipeid,	
		auth: localStorage.getItem("jwt")
		}), options)
		.then((response) => {
        if (response.ok) {
          window.location.href = "/";
        }
      })
      .catch((err) => {
        console.log(err);
      });
	}

	function review(e) {
		e.preventDefault();
		const options = {
			method: "POST",
			//headers: {
				//'Content-Type': 'application/json'
			//},
				//body: myRecipe.recipeid,
			};
		console.log(options);
		return fetch('https://skill-et-cooking-backend.herokuapp.com/recipes/'+ myRecipe.recipeid +'/review?' + new URLSearchParams({
		num: rating,	
		auth: localStorage.getItem("jwt")
		}), options)
		.then((response) => {
        if (response.ok) {
          setRated(true);
        }
      })
      .catch((err) => {
        console.log(err);
      });
	}

	useEffect(() => {
        checkIfOwnRecipe()

		const options = {
			method: "GET",
			headers: {
			  "Content-Type": "application/json",
			},
		  };
		  return fetch(
			'https://skill-et-cooking-backend.herokuapp.com/recipes/' + myRecipe.recipeid,
			options
		  )
	})

	console.log(myRecipe)
  function seeFromAuthor(){

    window.location.href = "/fromauthor/"+String(myRecipe.person.userid);
    /*return fetch('https://skill-et-cooking-backend.herokuapp.com/recipes?' + new URLSearchParams({
		userid: myRecipe.person.userid,	
		auth: localStorage.getItem("jwt")
		}))
		.then((response) => {
        if (response.ok) {
          history.pushState;
        }
      })
      .catch((err) => {
        console.log(err);
      });*/
    }


	return (
        <div>
		<Card>
            <h1>{myRecipe.name}  </h1>
            <h3>Pictures:</h3>
            <ul className="ulClass" list-style-type="none">
            <h3>{myRecipe.pictures.map(picture => (
					
                    <li key={picture.picnum} >
                        <img src={picture.picfilelocation} alt="new"/>
                    </li>
           ))}</h3>
            </ul>
            <h3>Average rating: {myRecipe.averageReview}</h3>
            <h3>Recipe duration: {myRecipe.duration} minutes</h3>
            <h3>Ingredients:</h3>
            <h3>{myRecipe.ingredients.map(ingredient => (
				<ul className="ulClass" key={ingredient.ingredientID}>
                    <li key={ingredient.ingredientID}>
                    {ingredient.ingredientName} {ingredient.amount}  
                    </li>
                </ul>
           ))}</h3>
            <h3>Steps:</h3>
            <h3>{myRecipe.steps.map(step => (
					<ul className="ulClass">
                    <li key={step.stepnum}>
                        <p>{step.stepnum}. {step.description}</p> 
                    </li>
					</ul>
           ))}</h3>
           <h3>This recipe was viewed {myRecipe.views} times.</h3>
		   <h3>Author: {myRecipe.person.username}</h3>
		   <div className="div1">
            {isMy && <Link to={{pathname:'/updateRecipe',
            updateRecipe:{myRecipe}}}><button>Edit Recipe</button></Link>}
			</div>
			
		   <div className="div1">

		   {localStorage.getItem("isMod")==1  &&
		   <button  type="submit" onClick = {onSubmit} >Delete</button>}
		   {localStorage.getItem("isMod")==0  && <Rating onClick={changeRating} initialRating={rating}/>} 
		   {localStorage.getItem("isMod")==0 &&
		   <button disabled={rating === 0}  type="submit" onClick ={review}>Rate this recipe!</button>}
       {rated && <h4>Recipe rated!</h4>}
            </div> 
       <div className="div1">
         <button onClick={seeFromAuthor}>See all recipes from this author</button>
	
       </div>
	   	 </Card>
		   {(localStorage.getItem("isMod") !== null) && <Disqus.DiscussionEmbed
            shortname={disqusShortname}
            config={disqusConfig}
          />}
      
      </div>
    )
	

}

export default RecipePage;