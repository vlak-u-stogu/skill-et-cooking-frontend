import React, { useRef, useState } from "react";
import Card from "./Card";
import "./AddRecipe.css";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";


function UpdateRecipe(props) {
  const history = useHistory();

  var toUpdate = props.location.updateRecipe.myRecipe;
  var sastojci = toUpdate.ingredients;
  const inputRefIngName = useRef(null);
  const inputRefStep = useRef(null);
  const inputRefIngAmount = useRef(null);
  var koraci = toUpdate.steps.map((el) => el.description);

  const [slike, setSlike] = useState(toUpdate.pictures);
  //console.log(slike)

  
  const [ingredients, setIngredients] = useState(sastojci);
  const [quantity, setQuantity] = useState([]);

  //console.log(ingredients)

  function addIngredient(event) {
    event.preventDefault();
    console.log(ingredients);
    const ingredient = document.getElementById("ingredient1").value;
    const quantiti = document.getElementById("quantity1").value;

    if (
      ingredients.find(
        (ingredientInState) => ingredientInState.ingredientName == ingredient
      )
    ) {
      setIngredients((prevIngredients) =>
        prevIngredients.map((ingredientInState) => {
          if (ingredientInState.ingredientName == ingredient) {
            return { ...ingredientInState, amount: quantiti };
          }
          return ingredientInState;
        })
      );
    } else {
		console.log("TU SAm")
      setIngredients((old) => [
        ...old,
        { ingredientName: ingredient, amount: quantiti },
      ]);
	  
    }
  }
  function removeIngredient(event) {
    event.preventDefault();
    var tmp = ingredients;
    const ingredient = document.getElementById("ingredient1").value;
    console.log(ingredient);

    setIngredients((prev) =>
      prev.filter(
        (ingredientInState) => ingredientInState.ingredientName !== ingredient
      )
    );
    console.log(ingredients);
  }

  const [steps, setSteps] = useState(koraci);

  function addStep(event) {
    event.preventDefault();
    console.log(steps);
    const step = document.getElementById("steps1").value;
    console.log(step);
    console.log(steps.indexOf(step));
    if (steps.indexOf(step) === -1) {
      console.log(steps);
      setSteps((old) => [...old, step]);
      console.log(steps);
    }
    console.log(steps);
  }
  function updateStep1(event) {
    event.preventDefault();
    var step = document.getElementById("steps1").value;
    console.log(korak);
    console.log(step);

    setSteps((prev) =>
      prev.map((stepInState) => {
        if (stepInState === korak) {
          return step;
        }
        return stepInState;
      })
    );
    console.log(steps);
  }

  function removeStep(event) {
    event.preventDefault();
    const step = document.getElementById("steps1").value;

    setSteps((prev) => prev.filter((stepInState) => stepInState !== step));
  }

  const [recipe, setRecipe] = useState({
    name: toUpdate.name,
    duration: String(toUpdate.duration),
  });

  const [images, setImages] = useState([]);
  var slikeZaDodat = [];
  var indZaUpdate;

  function addImages(e) {
    e.preventDefault();
    setImages([...e.target.files]);
    //console.log(images);
    //console.log(images);
  }

  function izracunajIndeks() {
    var popunjeno = [false, false, false, false, false];

    for (var i = 0; i < slike.length; i++) {
      popunjeno[slike[i].picnum] = true;
    }

    for (var j = 0; j < 5; j++) {
      if (popunjeno[j] === false) return j;
    }
    return 6;
  }

  function onSubmit(e) {
    e.preventDefault();

    const lista = [];

    for (let i = 0; i < ingredients.length; i++) {
      lista.push({
        name: ingredients[i].ingredientName,
        amount: ingredients[i].amount,
      });
    }

    console.log(ingredients);
    console.log(steps);

    const recipeData = {
      name: recipe.name,
      duration: recipe.duration,
      ingredients: lista,
      steps: steps,
    };
    console.log(recipeData);
    const data = new FormData();

    var indexiDodavanja = izracunajIndeks();
    console.log("INDEKS DODAVANJA");
    console.log(indexiDodavanja);

    data.append(
      "recipe",
      new Blob([JSON.stringify(recipeData)], { type: "application/json" })
    );
    data.append(indexiDodavanja, images[0]);

    const options = {
      method: "POST",
      body: data,
    };

    return fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/update?" +
        new URLSearchParams({
          auth: localStorage.getItem("jwt"),
          id: toUpdate.recipeid,
          update: [indexiDodavanja],
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          //history.push(`/RecipePage/` + toUpdate.recipeid);
          history.push("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function updateIngredient(event) {
    var ingrUpdate = event.target.getAttribute("data");
    var ingrUpdateAmount = event.target.getAttribute("amount");
    inputRefIngName.current.value = ingrUpdate;
    inputRefIngAmount.current.value = ingrUpdateAmount;

    console.log(ingrUpdate);
    console.log(ingrUpdateAmount);
  }
  var korak;

  function updateStep(event) {
    var ingrUpdate = event.target.getAttribute("data");
    inputRefStep.current.value = ingrUpdate;
    korak = ingrUpdate;

    console.log(ingrUpdate);
  }

  function onChange(event) {
    const { name, value } = event.target;
    setRecipe((oldRecipe) => ({ ...oldRecipe, [name]: value }));
  }

  function removeAllIngredients(event) {
    setIngredients((old) => []);
    setQuantity((old) => []);
  }

  function removeAllSteps(event) {
    setSteps((old) => []);
  }

  function removeAllImages(event) {
    setImages((old) => []);
  }

  async function updatePicture(event) {
    event.preventDefault();
    //console.log(slike)
    var pictureData = event.target.getAttribute("data");
    indZaUpdate = pictureData;
    console.log(pictureData);
    let updateData = new FormData();
    const lista = [];

    for (let i = 0; i < ingredients.length; i++) {
      lista.push({
        name: ingredients[i].ingredientName,
        amount: ingredients[i].amount,
      });
    }

    const recipeData = {
      name: recipe.name,
      duration: recipe.duration,
      ingredients: lista,
      steps: steps,
    };

    updateData.append(
      "recipe",
      new Blob([JSON.stringify(recipeData)], { type: "application/json" })
    );
    updateData.append(indZaUpdate, images[0]);
    const options = {
      method: "POST",
      body: updateData,
    };

    await fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/update?" +
        new URLSearchParams({
          auth: localStorage.getItem("jwt"),
          id: toUpdate.recipeid,
          update: [indZaUpdate],
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          window.location.href = "/";
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  var indexZaRemove;

  async function removePicture(event) {
    event.preventDefault();

    var pictureData = event.target.getAttribute("data");
    indexZaRemove = pictureData;
    console.log(pictureData);
    let updateData = new FormData();
    const lista = [];

    for (let i = 0; i < ingredients.length; i++) {
      lista.push({
        name: ingredients[i].ingredientName,
        amount: ingredients[i].amount,
      });
    }

    const recipeData = {
      name: recipe.name,
      duration: recipe.duration,
      ingredients: lista,
      steps: steps,
    };

    updateData.append(
      "recipe",
      new Blob([JSON.stringify(recipeData)], { type: "application/json" })
    );

    const options = {
      method: "POST",
      body: updateData,
    };

    await fetch(
      "https://skill-et-cooking-backend.herokuapp.com/recipes/update?" +
        new URLSearchParams({
          auth: localStorage.getItem("jwt"),
          id: toUpdate.recipeid,
          update: [indexZaRemove],
        }),
      options
    )
      .then((response) => {
        if (response.ok) {
          window.location.href = "/";
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <Card title="Update your recipe!">
      <div className="AddRecipe">
        <form onSubmit={onSubmit}>
          <div className="FormRow">
            <label>Name: </label>
            <input
              name="name"
              id="name1"
              onChange={onChange}
              value={toUpdate.name}
            />
          </div>
          <div className="FormRow">
            <label>Duration: </label>
            <input
              type="number"
              min="1"
              name="duration"
              id="duration1"
              onChange={onChange}
              value={toUpdate.duration}
            />
          </div>
          <div className="Row">
            <label>Ingredients: </label>
            <input
              name="ingredients"
              id="ingredient1"
              ref={inputRefIngName}
              required
            />
            <label>Ingredient quantity: </label>
            <input
              type="text"
              name="quantity"
              id="quantity1"
              ref={inputRefIngAmount}
            />
            <div className="row2">
              <button onClick={addIngredient}> Add ingredient</button>
              <button onClick={removeIngredient}> Remove ingredient</button>
              <button onClick={removeAllIngredients}> Remove all</button>
            </div>
            <div className="row2">
              <h1>{}</h1>
              <p>{ingredients.length !== 0 && "Current ingredients:"}</p>
              <p>
                {ingredients.map((el) => (
                  <span key={el.ingredientName}>
                    <button
                      onClick={updateIngredient}
                      data={el.ingredientName}
                      amount={el.amount}
                    >
                      {" "}
                      {el.ingredientName}{" "}
                    </button>
                  </span>
                ))}{" "}
              </p>
            </div>
          </div>
          <div className="FormRow">
            <label>Steps: </label>
            <textarea
              ref={inputRefStep}
              rows="15"
              cols="100"
              name="steps"
              id="steps1"
              required
            />
            <button onClick={addStep}> Add step</button>
            <button onClick={updateStep1}> Update step</button>
            <button onClick={removeStep}> Remove step</button>
            <button onClick={removeAllSteps}> Remove all</button>
            <div className="row2">
              <p>{steps.length !== 0 && "Current steps:"}</p>
              <p>
                {steps.map((el) => (
                  <span>
                    <button onClick={updateStep} data={el}>
                      {" "}
                      {el}{" "}
                    </button>
                  </span>
                ))}{" "}
              </p>
            </div>
          </div>
          <div className="Row1">
            <label>Image: </label>
            <input type="file" multiple accept="image/*" onChange={addImages} />
          </div>
          <div>
            <p>
              {slike.map((el) => (
                <div className="row2">
                  <img src={el.picfilelocation} alt=" "></img>
                  <button onClick={removePicture} data={el.picnum}>
                    Remove picture
                  </button>{" "}
                  <button onClick={updatePicture} data={el.picnum}>
                    Update picture
                  </button>{" "}
                </div>
              ))}
            </p>
          </div>
          <button type="submit" onClick={onSubmit}>
            Update recipe
          </button>
        </form>
      </div>
    </Card>
  );
}
export default UpdateRecipe;
