import React from "react";
import Card from "./Card";
import "./LogInForm.css";

function LogInForm(props) {
  const [form, setForm] = React.useState({ username: "", password: "" });
  const [loginFail, setLoginFail] = React.useState(false);

  function onChange(event) {
    const { name, value } = event.target;
    setForm((oldForm) => ({ ...oldForm, [name]: value }));
    console.log(form);
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      usernameORemail: form.username,
      password: form.password,
    };

    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    };

    return fetch("https://skill-et-cooking-backend.herokuapp.com/user/login", options)
      .then((response) => {
        if (response.ok) {
          setLoginFail(false);
          return response.json();
        } else {
          setLoginFail(true);
        }
      })
      .then((myJson) => {
        console.log(myJson)
        localStorage.setItem("jwt", myJson.jwt);
        localStorage.setItem("username", myJson.username);
        localStorage.setItem("isMod", myJson.isModerator);
        localStorage.setItem("expDate", myJson.expDate);

        window.location.href = "/";
      })
      .catch((e) => {
        console.log(e);
      });
  }
/*
  function isValid() {
    const { username, password } = form;
    return username.length >= 5 && username.length <= 30 && password.length >= 8;
  }
  */

  return (
    <Card title="Log in to continue">
      <div className="LogInForm">
        <form onSubmit={onSubmit}>
          <div className="FormRow">
            <label> Username or Email: </label>
            <input name="username" onChange={onChange} value={form.username} />
          </div>
          <div className="FormRow">
            <label> Password: </label>
            <input
              type="password"
              name="password"
              onChange={onChange}
              value={form.password}
            />
          </div>
          {loginFail && <p>Wrong username/email or password!</p>}
          <button type="submit" onClick={onSubmit} disabled={!(form.username.length >= 5 && form.username.length <= 30 && form.password.length >= 8)}>
            Log in
          </button>
        </form>
      </div>
    </Card>
  );
}

export default LogInForm;
