import React, {Component, useEffect, useState} from "react";
import { Link } from "react-router-dom";
import "./Recipes.css";
import Card from "./Card";

const Recipes = props => {
	
	const [recipesState, setRecipes] = useState(props);
	
	useEffect(() => {
		setRecipes(props.recipes);
		console.log(props.recipes[0]);
	  }, [props]);

	if (!Array.isArray(recipesState)) {
		return <div className="div123">No recipes loaded.</div>
	} else {
		if (recipesState.length == 0) 
		return <div className="div123">No recipes loaded.</div>
	return(
		<div  className="div123">
			<p className= "Recipes">
			<p>Recipe list:</p>
			 <ul>
				{recipesState.map(recipe => (

						
					 	<li key={recipe.id}>
							
							 {recipe.pictures[0] !== undefined && <img src={recipe.pictures[0]?.picfilelocation} />}
							 <Link to={{
								pathname: "/RecipePage/"+String(recipe.recipeid),
								state: recipe,
								}}>
								 {recipe.name} ({recipe.views} views)
							</Link>
                     	</li>
				))}
			</ul>
			</p>
		</div>
	)
}	
}

export default Recipes;